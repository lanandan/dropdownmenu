package com.example.sample;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

public class MainActivity extends Activity implements OnItemSelectedListener {

	Spinner custom_spinner_country,custom_spinner_state;
	String country, state;
	ArrayAdapter<String> madapter_cn, madapter_st;
	String countries[] = {"India", "England", "U.S.A", "Australia", "Europe"};
	String states[] = {"California", "Florida ","Georgia","New Jersey","New Mexico","New York","Pennsylvania ","Texas"}; 
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		custom_spinner_state = (Spinner)findViewById(R.id.sp_state);
		
		 custom_spinner_country = (Spinner)findViewById(R.id.sp_country);
		
		madapter_cn = new ArrayAdapter<String>(MainActivity.this, android.R.layout.simple_spinner_item, countries);
		madapter_st = new ArrayAdapter<String>(MainActivity.this, android.R.layout.simple_spinner_item, states);
		
		madapter_cn.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        custom_spinner_country.setAdapter(madapter_cn);
        custom_spinner_country.setOnItemSelectedListener(this);
        
        madapter_st.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        custom_spinner_state.setAdapter(madapter_st);
        custom_spinner_state.setOnItemSelectedListener(this);
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onItemSelected(AdapterView<?> parent, View view, int position,
			long id) {
		// TODO Auto-generated method stub
		
		country = custom_spinner_country.getSelectedItem().toString();
		state = custom_spinner_state.getSelectedItem().toString();
		
		Toast.makeText(getApplicationContext(), "You have selected : "+country+ " & " + state, Toast.LENGTH_SHORT).show();
		
	}

	@Override
	public void onNothingSelected(AdapterView<?> parent) {
		// TODO Auto-generated method stub
		Toast.makeText(getApplicationContext(), "You have selected nothing", Toast.LENGTH_SHORT).show();
	}
}
